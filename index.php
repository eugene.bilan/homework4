<?php

declare(strict_types=1);

use MyApp\CountLoop\CountLoop;
use MyApp\ErrorHandler;
use MyApp\User;
use MyApp\Date;
use MyApp\MyExceptionFirst;
use MyApp\MyExceptionSecond;

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Task  36.1 - 36.2
 */

//echo (new User('Tester', 'Testerov', 'Testovikovich'));
/**
 * Task  37.1 - 37.4
 */
//$Date = new Date();
//echo $Date->first_day = '03.02.2010';
//echo $Date->second_day = '12.02.2020';
//echo $Date->weekDay = date('l');
/**
 * Task  38.1 - 38.3
 */

//$MyExceptionFirst = new MyExceptionFirst();
//$MyExceptionSecond = new MyExceptionSecond();

/**
 * TASK - 41.1 - 41.3
 */
//$ErrorHandler = new ErrorHandler();
//$ErrorHandler->registerShutdown();

/**
 * TASK - 42.1 - 41.2
 */
//$count = new CountLoop();
//echo $count->checkLoop();