<?php

declare(strict_types=1);

namespace MyApp\MyExceptionFirst;

/**
 * Class MyExceptionFirst
 * @package MyApp\MyExceptionFirst
 */
class MyExceptionFirst extends \Exception {}

$customVariable = "";

try {
    if (empty($customVariable)) {
        throw new MyExceptionFirst("Variable is empty!");
    }
} catch(MyExceptionFirst $e) {
    error_log($e->getMessage() . PHP_EOL, 3, __DIR__. '/new_error.log');
}