<?php

declare(strict_types=1);

namespace MyApp\ErrorHandler;

/**
 * Class ErrorHandler
 * @package App\ErrorHandler
 */
class ErrorHandler
{
    /**
     *
     */
    public function registerShutdown(): void
    {
        register_shutdown_function(array(&$this, 'showErrors'));
    }

    /**
     *
     */
    public function showErrors(): void
    {
        $name = "Toms";

        try {
            if (empty($name)) {
                throw new \Exception('new error');
            }
            if ($name !== 'Tom') {
                throw new \Exception('Incorrect name');
            }
        } catch (\Exception $e) {
            error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/main_error.log');
        }
    }
}
