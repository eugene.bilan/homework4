<?php

declare(strict_types=1);

namespace MyApp\Date;

/**
 * Class Date
 * @package Date
 */
class Date
{
    /**
     * @var int
     */
    public int $year;

    /**
     * @var int
     */
    public int $month;

    /**
     * @var int
     */
    public int $day;

    /**
     * @param string $name
     */
    public function __get($name)
    {
        echo "Got: '$name'\n";
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        echo "Set: '$name' his value ";
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments) {
        echo "Вызов метода '$name' "
            . implode(', ', $arguments). "\n";
    }
}
