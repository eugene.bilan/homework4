<?php

declare(strict_types=1);

namespace MyApp\CountLoop;

/**
 * Class CountLoop
 * @package MyApp\CountLoop
 */
class CountLoop
{
    /**
     * @return float|string
     */
    public function checkLoop()
    {
        $start = microtime();
        for ($i = 0; $i < 10000; $i++) {
            if ($i === 5000) break;
        }
        $end = microtime();
        return $result = $end - $start;
    }
}
