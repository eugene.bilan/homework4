<?php

declare(strict_types=1);

namespace MyApp\MyExceptionSecond;

use MyApp\MyExceptionThird\MyExceptionThird;

/**
 * Class MyExceptionSecond
 * @package App\MyExceptionSecond
 */
class MyExceptionSecond extends MyExceptionThird
{

}

$customVariable = "";

try {
    if (empty($customVariable)) {
        throw new MyExceptionSecond("MyExceptionSecond problem!");
    }
} catch (MyExceptionSecond $e) {
    echo "MyExceptionSecond " . $e->getTrace();
    error_log($e->getTrace() . PHP_EOL, 3, __DIR__ . '/new_error.log');
    throw $e;
}
