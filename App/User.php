<?php

declare(strict_types=1);

namespace MyApp\User;

/**
 * Class User
 * @package User
 */
class User
{
    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $surname;

    /**
     * @var string
     */
    public string $patronymic;

    /**
     * User constructor.
     * @param string $name
     * @param string $surname
     * @param string $patronymic
     */
    public function __construct(string $name, string $surname, string $patronymic)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }
}
